package screens;

import battleship.client.GameLogic;
import battleship.client.Location;
import battleship.client.Ship;
import battleship.client.hasTheGameStarted;
import battleship.client.serverFunctions;
import battleship.client.tiktak;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

/**
 * this class is UI of the game
 *
 * @author Eden
 */
public class gameScreen extends javax.swing.JFrame {

    private final int WIDTH = 10;
    private final int LENGHT = 10;

    private int pointX;
    private int pointY;
    private int indexI;
    private int indexJ;
    private boolean itIsMyTurn;
    private Thread clock;
    private JLabel shipSelected;
    private JLabel[] shipsIMG;
    private int isAllshipOrders;
    private Ship[] allShips;
    private serverFunctions server;
    private Ship ship;
    
    public JLabel[][] myBoard;
    public JLabel[][] GuessBoard;
    public GameLogic logic;
    public Thread hello;
    public String guestName;
    public int myID;
    public int gameID;
    public Thread gameStartedd;
    public boolean theTameHasStarted;
    

    public gameScreen() {

        initComponents();
        start();

    }

    /**
     * Initializes the board
     */
    private void start() {
        ImageIcon imgThisImg = new ImageIcon(getClass().getResource("/icons/applicationIcon.jpg"));
        this.setIconImage(imgThisImg.getImage());
        this.theTameHasStarted = false;
        this.itIsMyTurn = true;
        this.server = new serverFunctions();
        this.setLocationRelativeTo(null);
        this.setSize(1280, 720);
        this.myBoard = new JLabel[10][10];
        this.ship = new Ship();
        this.allShips = new Ship[5];
        this.logic = new GameLogic(this);
        this.isAllshipOrders = 0;
        for (int i = 0; i < allShips.length; i++) {
            allShips[i] = new Ship(ship);
        }

        this.shipsIMG = new JLabel[5];
        this.shipsIMG[0] = this.shipA;
        this.shipsIMG[1] = this.shipB;
        this.shipsIMG[2] = this.shipC;
        this.shipsIMG[3] = this.shipD;
        this.shipsIMG[4] = this.shipE;

        createSubmarineBoard();
        setImageArrow();

    }

    public void close() {
        this.dispose();
    }

    public void startClock() {
        stopClock();
        this.clock = new Thread(new tiktak(this, server));
        this.clock.start();
    }

    public void stopClock() {
        if (this.clock != null) {
            this.clock.stop();
        }
    }

    private void setImageArrow() {
        logic.hideDirectionArrows();
    }

    public void createGuessBoard() {
        GuessBoard = new JLabel[WIDTH][LENGHT];
        int x;
        int y;

        for (indexI = 0, x = 760; indexI < WIDTH; indexI++, x += 48) {
            for (indexJ = 0, y = 100; indexJ < LENGHT; indexJ++, y += 48) {
                GuessBoard[indexI][indexJ] = new JLabel();
                GuessBoard[indexI][indexJ].setSize(48, 48);
                GuessBoard[indexI][indexJ].setLocation(x, y);
                GuessBoard[indexI][indexJ].setOpaque(true);
                GuessBoard[indexI][indexJ].setBorder(
                        BorderFactory.createLineBorder(new Color(162, 207, 149), 1));
                GuessBoard[indexI][indexJ].setForeground(Color.white);
                GuessBoard[indexI][indexJ].setBackground(new Color(16, 32, 57, 100));
                this.lowerLabel.add(GuessBoard[indexI][indexJ]);
                GuessBoard[indexI][indexJ].addMouseListener(new MouseAdapter() {

                    int i = indexI;
                    int j = indexJ;

                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (!isItIsMyTurn()) {
                            logic.showMsg(Color.ORANGE, "IT IS NOT YOUR TURN");
                            return;
                        }

                        gameScreen.this.logic.guessLocation(i, j);
                    }
                });
                repaint();
            }
        }
        repaint();
        //SwingUtilities.updateComponentTreeUI(gameScreen.this);
        //SwingUtilities.updateComponentTreeUI(this);
    }

    /**
     * Builds the submarine board
     */
    private void createSubmarineBoard() {
        int x;
        int y;
        for (indexI = 0, x = 50; indexI < 10; indexI++, x += 48) {
            for (indexJ = 0, y = 100; indexJ < 10; indexJ++, y += 48) {
                myBoard[indexI][indexJ] = new JLabel();
                myBoard[indexI][indexJ].setSize(48, 48);
                myBoard[indexI][indexJ].setLocation(x, y);
                myBoard[indexI][indexJ].setOpaque(true);

                myBoard[indexI][indexJ].setBorder(
                        BorderFactory.createLineBorder(new Color(162, 207, 149), 1));
                //myBoard[indexI][indexJ].setForeground(Color.white);
                myBoard[indexI][indexJ].setBackground(new Color(16, 32, 57, 100));
                this.lowerLabel.add(myBoard[indexI][indexJ]);

                myBoard[indexI][indexJ].addMouseListener(new MouseAdapter() {

                    int i = indexI;
                    int j = indexJ;

                    @Override
                    public void mouseClicked(MouseEvent e) {
                        //System.out.println("(" + i + "," + j + ")");

                        if (theTameHasStarted) {
                            return;
                        }
                        ship.setPointx(i);
                        ship.setPointy(j);
                        if (!logic.showValidArrows(ship)) {
                            String msg = "";
                            if (ship.getShipID() == -1) {
                                msg = "PLEASE SELECT A SHIP";
                            } else {
                                msg = "THIS IS NOT A LEGAL PLACE";
                            }
                            logic.showMsg(Color.ORANGE, msg);
                        }
                        SwingUtilities.updateComponentTreeUI(gameScreen.this);

                    }
                });
            }
        }
    }

    public void markEnemyMoveOnSubmarineBoard(Location lc, boolean isHit) {
        ImageIcon img;
        if (isHit) {
            img = new ImageIcon(getClass().getResource("/icons/hit.jpg"));
            this.setItIsMyTurn(false);
            this.turn.setText("NOT YOUR TURN");
        } else {
            img = new ImageIcon(getClass().getResource("/icons/miss.jpg"));
            this.setItIsMyTurn(true);
            this.turn.setBackground(new Color(162, 207, 149));
            this.turn.setText("YOUR TURN");
        }

        myBoard[lc.getPointx()][lc.getPointy()].setIcon(img);
    }

    public void putSubmarineOnBoard(int direction) {

        this.isAllshipOrders++;
        logic.showSubmarineOnBoard(ship, direction);
        logic.hideDirectionArrows();
        this.allShips[ship.getShipID() - 101] = new Ship(ship);
        this.shipsIMG[ship.getShipID() - 101] = null;

        if (this.isAllshipOrders == 5) {
            startPlay();
            LblTest.setVisible(false);
        }

        this.lowerPanel.remove(this.shipSelected);
        ship.setShipID(-1);
        SwingUtilities.updateComponentTreeUI(this);
    }

    public boolean isItIsMyTurn() {
        return itIsMyTurn;
    }

    public void setItIsMyTurn(boolean itIsMyTurn) {
        this.itIsMyTurn = itIsMyTurn;
        startClock();
    }

    /**
     * Closes the game
     */
    public void exit() {
        if (logic.updateGame != null) {
            logic.updateGame.stop();
        }
        if (this.gameStartedd != null) {
            this.gameStartedd.stop();
        }
        if (this.hello != null) {
            this.hello.stop();
        }
        stopClock();

        this.server.FinishedGame(gameID);
        this.server.logout(myID, this.server.getip(), this.server.getmac());
        this.dispose();

    }

    private void intiShip(int shipID) {

        List<ImageIcon> ImagesToShow = new ArrayList<ImageIcon>();
        switch (shipID) {
            case 101:
                this.shipSelected = shipA;

                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipA1.png")));
                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipA2.png")));

                this.ship.setSize(2);
                this.ship.setImage(new ImageIcon(getClass().getResource("/icons/shipA.png")));
                this.ship.setIsInLife(true);
                this.ship.setShipID(101);
                this.ship.setImagesToShow(ImagesToShow);

                this.allShips[shipID - 101] = ship;

                break;

            case 102:
                this.shipSelected = shipB;

                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipB1.png")));
                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipB2.png")));
                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipB3.png")));

                this.ship.setSize(3);
                this.ship.setImage(new ImageIcon(getClass().getResource("/icons/shipB.png")));
                this.ship.setIsInLife(true);
                this.ship.setShipID(102);
                this.ship.setImagesToShow(ImagesToShow);

                this.allShips[shipID - 101] = ship;

                break;

            case 103:

                this.shipSelected = shipC;

                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipC1.png")));
                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipC2.png")));
                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipC3.png")));
                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipC4.png")));

                this.ship.setSize(4);
                this.ship.setImage(new ImageIcon(getClass().getResource("/icons/shipC.png")));
                this.ship.setIsInLife(true);
                this.ship.setShipID(103);
                this.ship.setImagesToShow(ImagesToShow);

                this.allShips[shipID - 101] = ship;

                break;

            case 104:

                this.shipSelected = shipD;

                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipC1.png")));
                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipC2.png")));
                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipC3.png")));
                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipC4.png")));

                this.ship.setSize(4);
                this.ship.setImage(new ImageIcon(getClass().getResource("/icons/shipC.png")));
                this.ship.setIsInLife(true);
                this.ship.setShipID(104);
                this.ship.setImagesToShow(ImagesToShow);

                this.allShips[shipID - 101] = ship;

                break;

            case 105:

                this.shipSelected = shipE;

                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipD1.png")));
                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipD2.png")));
                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipD3.png")));
                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipD4.png")));
                ImagesToShow.add(new ImageIcon(getClass().getResource("/icons/shipD5.png")));

                this.ship.setSize(5);
                this.ship.setImage(new ImageIcon(getClass().getResource("/icons/shipD.png")));
                this.ship.setIsInLife(true);
                this.ship.setShipID(105);
                this.ship.setImagesToShow(ImagesToShow);

                this.allShips[shipID - 101] = ship;

                break;
        }
    }

    public void startPlay() {
        String x = logic.setMyBaord();

        gameStartedd = new Thread(new hasTheGameStarted(logic));
        gameStartedd.start();
        this.lowerPanel.remove(LblTest);
        stopClock();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        upperPanel = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        LblEnemyName = new javax.swing.JLabel();
        LblmyName = new javax.swing.JLabel();
        LblVS = new javax.swing.JLabel();
        lowerPanel = new javax.swing.JPanel();
        up = new javax.swing.JLabel();
        right = new javax.swing.JLabel();
        left = new javax.swing.JLabel();
        down = new javax.swing.JLabel();
        shipA = new javax.swing.JLabel();
        shipD = new javax.swing.JLabel();
        shipE = new javax.swing.JLabel();
        shipB = new javax.swing.JLabel();
        shipC = new javax.swing.JLabel();
        turn = new javax.swing.JLabel();
        loadingPnl1 = new icons.loadingPnl();
        clockImage = new javax.swing.JLabel();
        LblTest = new javax.swing.JLabel();
        lowerLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        upperPanel.setBackground(new java.awt.Color(44, 62, 80));
        upperPanel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                upperPanelMouseDragged(evt);
            }
        });
        upperPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                upperPanelMousePressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 51, 51));
        jLabel5.setText("X");
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 51, 51));
        jLabel6.setText(" - ");
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel6MouseClicked(evt);
            }
        });

        LblEnemyName.setFont(new java.awt.Font("Bernard MT Condensed", 1, 30)); // NOI18N
        LblEnemyName.setForeground(new java.awt.Color(162, 207, 149));
        LblEnemyName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblEnemyName.setText("game");

        LblmyName.setFont(new java.awt.Font("Bernard MT Condensed", 1, 30)); // NOI18N
        LblmyName.setForeground(new java.awt.Color(162, 207, 149));
        LblmyName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblmyName.setText("game");

        LblVS.setFont(new java.awt.Font("Bernard MT Condensed", 1, 30)); // NOI18N
        LblVS.setForeground(new java.awt.Color(162, 207, 149));
        LblVS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblVS.setText("VS");

        javax.swing.GroupLayout upperPanelLayout = new javax.swing.GroupLayout(upperPanel);
        upperPanel.setLayout(upperPanelLayout);
        upperPanelLayout.setHorizontalGroup(
            upperPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, upperPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(LblmyName, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(186, 186, 186)
                .addComponent(LblVS, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(186, 186, 186)
                .addComponent(LblEnemyName, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(132, 132, 132)
                .addComponent(jLabel6)
                .addGap(25, 25, 25)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
        );
        upperPanelLayout.setVerticalGroup(
            upperPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, upperPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(upperPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(upperPanelLayout.createSequentialGroup()
                        .addGroup(upperPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(LblEnemyName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(LblmyName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(LblVS, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(5, 5, 5))
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(29, 29, 29))
        );

        lowerPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        up.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/‏‏greenArrowUp.png"))); // NOI18N
        up.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                upMouseClicked(evt);
            }
        });
        lowerPanel.add(up, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 196, -1, -1));

        right.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/greenArrowRight.png"))); // NOI18N
        right.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rightMouseClicked(evt);
            }
        });
        lowerPanel.add(right, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 250, -1, -1));

        left.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/‏‏‏‏greenArrowLeft.png"))); // NOI18N
        left.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                leftMouseClicked(evt);
            }
        });
        lowerPanel.add(left, new org.netbeans.lib.awtextra.AbsoluteConstraints(545, 246, -1, -1));
        left.getAccessibleContext().setAccessibleName("jLabel4");

        down.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/‏‏‏‏greenArrowDown.png"))); // NOI18N
        down.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                downMouseClicked(evt);
            }
        });
        lowerPanel.add(down, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 305, -1, -1));

        shipA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/shipA.png"))); // NOI18N
        shipA.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                shipAMouseClicked(evt);
            }
        });
        lowerPanel.add(shipA, new org.netbeans.lib.awtextra.AbsoluteConstraints(915, 161, -1, -1));

        shipD.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/shipC.png"))); // NOI18N
        shipD.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                shipDMouseClicked(evt);
            }
        });
        lowerPanel.add(shipD, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 350, -1, -1));

        shipE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/shipD.png"))); // NOI18N
        shipE.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                shipEMouseClicked(evt);
            }
        });
        lowerPanel.add(shipE, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 221, -1, -1));

        shipB.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/shipB.png"))); // NOI18N
        shipB.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                shipBMouseClicked(evt);
            }
        });
        lowerPanel.add(shipB, new org.netbeans.lib.awtextra.AbsoluteConstraints(1144, 123, -1, -1));

        shipC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/shipC.png"))); // NOI18N
        shipC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                shipCMouseClicked(evt);
            }
        });
        lowerPanel.add(shipC, new org.netbeans.lib.awtextra.AbsoluteConstraints(1144, 352, -1, -1));

        turn.setBackground(new java.awt.Color(0, 0, 0));
        turn.setFont(new java.awt.Font("Bernard MT Condensed", 0, 24)); // NOI18N
        turn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lowerPanel.add(turn, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 600, 1280, 40));

        loadingPnl1.setOpaque(false);

        clockImage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        clockImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/radarIMG.png"))); // NOI18N

        javax.swing.GroupLayout loadingPnl1Layout = new javax.swing.GroupLayout(loadingPnl1);
        loadingPnl1.setLayout(loadingPnl1Layout);
        loadingPnl1Layout.setHorizontalGroup(
            loadingPnl1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loadingPnl1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(clockImage, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        loadingPnl1Layout.setVerticalGroup(
            loadingPnl1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(clockImage, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        lowerPanel.add(loadingPnl1, new org.netbeans.lib.awtextra.AbsoluteConstraints(545, 16, 200, 180));

        LblTest.setBackground(new java.awt.Color(162, 207, 149));
        LblTest.setFont(new java.awt.Font("Bernard MT Condensed", 1, 24)); // NOI18N
        LblTest.setForeground(new java.awt.Color(162, 207, 149));
        LblTest.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblTest.setText("RANDOM ORDER");
        LblTest.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblTestMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblTestMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblTestMouseExited(evt);
            }
        });
        lowerPanel.add(LblTest, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 480, 180, 60));

        lowerLabel.setBackground(new java.awt.Color(0, 0, 0));
        lowerLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/backgroungGameScreen.jpg"))); // NOI18N
        lowerLabel.setOpaque(true);
        lowerPanel.add(lowerLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(-3, -8, 1290, 650));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lowerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 1281, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(upperPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(88, 88, 88)
                .addComponent(lowerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 640, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(upperPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
        exit();
    }//GEN-LAST:event_jLabel5MouseClicked

    private void jLabel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseClicked

        // min this window
        this.setState(JFrame.ICONIFIED);
    }//GEN-LAST:event_jLabel6MouseClicked

    private void upperPanelMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_upperPanelMouseDragged

        // move this screen
        this.setLocation(this.getLocation().x + evt.getX() - pointX,
                this.getLocation().y + evt.getY() - pointY);
    }//GEN-LAST:event_upperPanelMouseDragged

    private void upperPanelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_upperPanelMousePressed

        // get point of this screen
        pointX = evt.getX();
        pointY = evt.getY();
    }//GEN-LAST:event_upperPanelMousePressed

    private void shipAMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_shipAMouseClicked

        intiShip(101);

    }//GEN-LAST:event_shipAMouseClicked

    private void shipBMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_shipBMouseClicked

        intiShip(102);

    }//GEN-LAST:event_shipBMouseClicked

    private void shipCMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_shipCMouseClicked

        intiShip(103);

    }//GEN-LAST:event_shipCMouseClicked

    private void shipDMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_shipDMouseClicked

        intiShip(104);

    }//GEN-LAST:event_shipDMouseClicked

    private void upMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_upMouseClicked
        putSubmarineOnBoard(logic.UP);
    }//GEN-LAST:event_upMouseClicked

    private void leftMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leftMouseClicked
        putSubmarineOnBoard(logic.LEFT);
    }//GEN-LAST:event_leftMouseClicked

    private void rightMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rightMouseClicked
        putSubmarineOnBoard(logic.RIGHT);
    }//GEN-LAST:event_rightMouseClicked

    private void downMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_downMouseClicked
        putSubmarineOnBoard(logic.DOWN);
    }//GEN-LAST:event_downMouseClicked

    private void shipEMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_shipEMouseClicked

        intiShip(105);

    }//GEN-LAST:event_shipEMouseClicked

    private void LblTestMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblTestMouseClicked

        Random rand = new Random();
        boolean isDone;
        int i;
        int j;

        for (int ship = 0; ship < this.shipsIMG.length; ship++) {

            if (this.shipsIMG[ship] == null) {
                continue;
            }

            isDone = false;

            while (!isDone) {
                intiShip(ship + 101);
                i = rand.nextInt(10);
                j = rand.nextInt(10);
                this.allShips[ship].setPointx(i);
                this.allShips[ship].setPointy(j);

                isDone = this.logic.putShipInRandumLocation(this.allShips[ship]);
            }
            this.lowerPanel.remove(this.shipsIMG[ship]);
            SwingUtilities.updateComponentTreeUI(this);
        }
        startPlay();
    }//GEN-LAST:event_LblTestMouseClicked

    private void LblTestMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblTestMouseEntered
        LblTest.setForeground(Color.red);
    }//GEN-LAST:event_LblTestMouseEntered

    private void LblTestMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblTestMouseExited
        LblTest.setForeground(new Color(162, 207, 149));
    }//GEN-LAST:event_LblTestMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(gameScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(gameScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(gameScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(gameScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new gameScreen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JLabel LblEnemyName;
    private javax.swing.JLabel LblTest;
    private javax.swing.JLabel LblVS;
    public javax.swing.JLabel LblmyName;
    private javax.swing.JLabel clockImage;
    public javax.swing.JLabel down;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    public javax.swing.JLabel left;
    public icons.loadingPnl loadingPnl1;
    public javax.swing.JLabel lowerLabel;
    public javax.swing.JPanel lowerPanel;
    public javax.swing.JLabel right;
    private javax.swing.JLabel shipA;
    private javax.swing.JLabel shipB;
    private javax.swing.JLabel shipC;
    private javax.swing.JLabel shipD;
    private javax.swing.JLabel shipE;
    public javax.swing.JLabel turn;
    public javax.swing.JLabel up;
    private javax.swing.JPanel upperPanel;
    // End of variables declaration//GEN-END:variables

}
