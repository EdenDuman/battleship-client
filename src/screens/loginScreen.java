package screens;

import battleship.client.serverFunctions;
import battleship.client.seyHello;
import com.sbix.jnotify.NPosition;
import com.sbix.jnotify.NoticeWindow;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class loginScreen extends javax.swing.JFrame {

    private serverFunctions server;
    private String ip;
    private String mac;
    private int id;
    private int pointX;
    private int pointY;
    public Thread hello;
    private boolean connectionFlag;

    private void start() {
        ImageIcon imgThisImg = new ImageIcon(getClass().getResource("/icons/applicationIcon.jpg"));
        this.setIconImage(imgThisImg.getImage());
        this.server = new serverFunctions();
        this.ip = this.server.getip();
        this.mac = this.server.getmac();
        this.id = 0;
        this.pointX = 0;
        this.pointY = 0;
        this.hello = null;
        this.connectionFlag = false;
    }

    private void exit() {

        if (this.id > 0) {
            hello.interrupt();
            server.logout(id, ip, mac);
        }

        this.dispose();

    }

    private void clear() {
        txtEmail.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
        txtPassword.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
    }

    private boolean checkdata() {

        boolean flag = true;

        if (txtEmail.getText().trim().equals("")) {
            txtEmail.setBorder(BorderFactory.createLineBorder(Color.RED,1));
            flag = false;
        }

        if (txtPassword.getText().trim().equals("")) {
            txtPassword.setBorder(BorderFactory.createLineBorder(Color.RED,1));
            flag = false;
        }
        
        return flag;
    }
    
    private void checkResult(){
        
        String message = "";
        switch(this.id){
            case -1:
                message = "The user does not exist";
                break;
            case -2:
                message = "You're connected from somewhere else\n" +
                            "Wait 15 seconds and try again";
                break;
            case -3:
                message = "communication problem";
                break;
        }
        if(this.id < 0){
            new NoticeWindow(Color.red, message, NoticeWindow.SHORT_DELAY, NPosition.CENTER);
        }
    }
    
    public void messageOnScreen(String message){
        new NoticeWindow(Color.blue, message, NoticeWindow.SHORT_DELAY, NPosition.CENTER);
    }

    public loginScreen() {
        start();
        initComponents();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        LblMail = new javax.swing.JLabel();
        lbltPassword = new javax.swing.JLabel();
        txtPassword = new javax.swing.JPasswordField();
        txtEmail = new javax.swing.JTextField();
        btnSubmit = new javax.swing.JButton();
        LblRegister = new javax.swing.JLabel();
        lblForgotPassword = new javax.swing.JLabel();
        eden = new javax.swing.JButton();
        samuel = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(44, 62, 80));
        jPanel2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel2MouseDragged(evt);
            }
        });
        jPanel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel2MousePressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 51, 51));
        jLabel5.setText("X");
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 51, 51));
        jLabel6.setText(" - ");
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel6MouseClicked(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Bernard MT Condensed", 0, 30)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(162, 207, 149));
        jLabel1.setText("log-in");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addGap(25, 25, 25)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(5, 5, 5))
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(29, 29, 29))
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 750, -1));

        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblMail.setFont(new java.awt.Font("Bernard MT Condensed", 0, 18)); // NOI18N
        LblMail.setForeground(new java.awt.Color(162, 207, 149));
        LblMail.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMail.setText("EMAIL");
        LblMail.setToolTipText("");
        jPanel3.add(LblMail, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 150, 100, 50));

        lbltPassword.setFont(new java.awt.Font("Bernard MT Condensed", 0, 18)); // NOI18N
        lbltPassword.setForeground(new java.awt.Color(162, 207, 149));
        lbltPassword.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbltPassword.setText("PASSWORD");
        lbltPassword.setToolTipText("");
        jPanel3.add(lbltPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 240, 100, 50));

        txtPassword.setFont(new java.awt.Font("Bernard MT Condensed", 0, 18)); // NOI18N
        txtPassword.setForeground(new java.awt.Color(162, 207, 149));
        txtPassword.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPassword.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtPassword.setOpaque(false);
        jPanel3.add(txtPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 240, 260, 50));

        txtEmail.setFont(new java.awt.Font("Bernard MT Condensed", 1, 20)); // NOI18N
        txtEmail.setForeground(new java.awt.Color(162, 207, 149));
        txtEmail.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtEmail.setToolTipText("");
        txtEmail.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtEmail.setOpaque(false);
        jPanel3.add(txtEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 150, 260, 50));

        btnSubmit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/‏‏submitIMG.png"))); // NOI18N
        btnSubmit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSubmitMouseClicked(evt);
            }
        });
        jPanel3.add(btnSubmit, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 380, 130, 60));

        LblRegister.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/‏‏registerIMG.png"))); // NOI18N
        LblRegister.setOpaque(true);
        LblRegister.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblRegisterMouseClicked(evt);
            }
        });
        jPanel3.add(LblRegister, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 380, 130, 60));

        lblForgotPassword.setFont(new java.awt.Font("Bernard MT Condensed", 0, 16)); // NOI18N
        lblForgotPassword.setForeground(new java.awt.Color(162, 207, 149));
        lblForgotPassword.setText("forgot your password?");
        lblForgotPassword.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblForgotPasswordMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblForgotPasswordMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblForgotPasswordMouseExited(evt);
            }
        });
        jPanel3.add(lblForgotPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 310, -1, -1));

        eden.setText("eden");
        eden.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                edenMouseClicked(evt);
            }
        });
        jPanel3.add(eden, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 110, 80, -1));

        samuel.setText("samuel");
        samuel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                samuelMouseClicked(evt);
            }
        });
        jPanel3.add(samuel, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 70, 80, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/backgroundScreen.jpg"))); // NOI18N
        jPanel3.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, -10, 750, 480));

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 88, 750, 470));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 750, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 555, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSubmitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubmitMouseClicked
        
        if (this.connectionFlag){
            return;
        }
        
        clear();
        if (checkdata()) {
            
            String mail = txtEmail.getText().trim();
            String password = txtPassword.getText().trim();
            
            this.id = server.login(ip, mac, password, mail);
            
            checkResult();
            
            if (this.id > 0){
                hello = new Thread(new seyHello(this.id));
                hello.start();
                this.connectionFlag = true;
                this.btnSubmit.setEnabled(false);
                this.LblRegister.setEnabled(false);
                this.lblForgotPassword.setEnabled(false);
                mainScreen ms = new mainScreen();
                ms.id = this.id;
                ms.hello = this.hello;
                ms.setName();
                ms.setVisible(true);
                this.dispose();
            }
            
        }

    }//GEN-LAST:event_btnSubmitMouseClicked

    private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
        exit();
    }//GEN-LAST:event_jLabel5MouseClicked

    private void jPanel2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MousePressed

        // get point of this screen
        pointX = evt.getX();
        pointY = evt.getY();
    }//GEN-LAST:event_jPanel2MousePressed

    private void jPanel2MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseDragged

        // move this screen 
        this.setLocation(this.getLocation().x + evt.getX() - pointX,
                this.getLocation().y + evt.getY() - pointY);
    }//GEN-LAST:event_jPanel2MouseDragged

    private void jLabel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseClicked

        // min this window
        this.setState(JFrame.ICONIFIED);
    }//GEN-LAST:event_jLabel6MouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        exit();
    }//GEN-LAST:event_formWindowClosing

    private void LblRegisterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblRegisterMouseClicked
        
        if (this.connectionFlag){
            return;
        }
        
        registerScreen rs = new registerScreen();
        rs.setVisible(true);
        exit();
    }//GEN-LAST:event_LblRegisterMouseClicked

    private void lblForgotPasswordMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblForgotPasswordMouseClicked
        
        if (this.connectionFlag){
            return;
        }
        
        forgotPassword fp = new forgotPassword();
        fp.setVisible(true);
        exit();
    }//GEN-LAST:event_lblForgotPasswordMouseClicked

    private void samuelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_samuelMouseClicked
        
        txtEmail.setText("samuel.martiano1@gmail.com");
        txtPassword.setText("FE8A7");
        btnSubmitMouseClicked(null);
        
    }//GEN-LAST:event_samuelMouseClicked

    private void edenMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_edenMouseClicked
        
        txtEmail.setText("EdenDuman7@gmail.com");
        txtPassword.setText("E2D74");
        btnSubmitMouseClicked(null);
        
    }//GEN-LAST:event_edenMouseClicked

    private void lblForgotPasswordMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblForgotPasswordMouseEntered
        lblForgotPassword.setForeground(Color.red);
    }//GEN-LAST:event_lblForgotPasswordMouseEntered

    private void lblForgotPasswordMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblForgotPasswordMouseExited
        lblForgotPassword.setForeground(new Color(162,207,149));
    }//GEN-LAST:event_lblForgotPasswordMouseExited

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(loginScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(loginScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(loginScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(loginScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new loginScreen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LblMail;
    private javax.swing.JLabel LblRegister;
    private javax.swing.JButton btnSubmit;
    private javax.swing.JButton eden;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblForgotPassword;
    private javax.swing.JLabel lbltPassword;
    private javax.swing.JButton samuel;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JPasswordField txtPassword;
    // End of variables declaration//GEN-END:variables
}
