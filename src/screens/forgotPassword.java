package screens;

import battleship.client.serverFunctions;
import com.sbix.jnotify.NPosition;
import com.sbix.jnotify.NoticeWindow;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class forgotPassword extends javax.swing.JFrame {

    private serverFunctions server;
    private String ip;
    private String mac;
    private int pointX;
    private int pointY;
    private boolean flag;
    private int id;

    private void start() {

        ImageIcon imgThisImg = new ImageIcon(getClass().getResource("/icons/applicationIcon.jpg"));
        this.setIconImage(imgThisImg.getImage());
        this.server = new serverFunctions();
        this.ip = this.server.getip();
        this.mac = this.server.getmac();
        this.pointX = 0;
        this.pointY = 0;
        this.flag = false;
        this.id = 0;
    }

    private void exit() {

        this.dispose();

    }

    private void clear() {

        lblStatus.setText("");

        txtEmail.setBackground(null);

    }

    private boolean checkdata() {

        boolean flag = true;

        if (txtEmail.getText().trim().equals("")) {
            txtEmail.setBackground(Color.red);
            flag = false;
        }

        return flag;
    }

    private void checkResult(int result) {

        String message = "";
        switch (result) {
            case -1:
                message = "Email does not exist on system";
                break;
            case -2:
                message = "Problem sending email";
                break;

        }
        if (result < 0) {
            new NoticeWindow(Color.red, message, NoticeWindow.SHORT_DELAY, NPosition.CENTER);
        }
    }

    public forgotPassword() {
        start();
        initComponents();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        lblbMin = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        LblMail = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        btnSubmit = new javax.swing.JButton();
        lblStatus = new javax.swing.JLabel();
        LblRegister = new javax.swing.JLabel();
        lblLogin = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(44, 62, 80));
        jPanel2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel2MouseDragged(evt);
            }
        });
        jPanel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel2MousePressed(evt);
            }
        });
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 51, 51));
        jLabel5.setText("X");
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
        });
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 10, 33, 49));

        lblbMin.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lblbMin.setForeground(new java.awt.Color(255, 51, 51));
        lblbMin.setText(" - ");
        lblbMin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblbMinMouseClicked(evt);
            }
        });
        jPanel2.add(lblbMin, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 10, -1, -1));

        jLabel1.setFont(new java.awt.Font("Bernard MT Condensed", 1, 30)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(162, 207, 149));
        jLabel1.setText("forgot password");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 220, 40));

        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblMail.setFont(new java.awt.Font("Bernard MT Condensed", 0, 18)); // NOI18N
        LblMail.setForeground(new java.awt.Color(162, 207, 149));
        LblMail.setText("Enter you'r Email");
        LblMail.setToolTipText("");
        jPanel3.add(LblMail, new org.netbeans.lib.awtextra.AbsoluteConstraints(111, 210, 150, 50));

        txtEmail.setFont(new java.awt.Font("Bernard MT Condensed", 1, 20)); // NOI18N
        txtEmail.setForeground(new java.awt.Color(162, 207, 149));
        txtEmail.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtEmail.setToolTipText("");
        txtEmail.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtEmail.setOpaque(false);
        jPanel3.add(txtEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 210, 260, 50));

        btnSubmit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/‏‏submitIMG.png"))); // NOI18N
        btnSubmit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSubmitMouseClicked(evt);
            }
        });
        jPanel3.add(btnSubmit, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 280, 130, 60));
        jPanel3.add(lblStatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(218, 352, 108, 35));

        LblRegister.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/‏‏registerIMG.png"))); // NOI18N
        LblRegister.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblRegisterMouseClicked(evt);
            }
        });
        jPanel3.add(LblRegister, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 370, 130, 60));

        lblLogin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/loginIMG.png"))); // NOI18N
        lblLogin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblLoginMouseClicked(evt);
            }
        });
        jPanel3.add(lblLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 370, 130, 60));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/backgroundScreen.jpg"))); // NOI18N
        jPanel3.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, -4, 750, 480));

        jPanel2.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 80, 750, 480));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSubmitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubmitMouseClicked

        if (this.flag) {
            return;
        }
        clear();
        if (checkdata()) {

            String mail = txtEmail.getText().trim();

            int result = server.resetPassword(mail, ip, mac);

            checkResult(result);

            if (result == 1) {

                this.btnSubmit.setEnabled(false);
                loginScreen ls = new loginScreen();
                ls.setVisible(true);
                ls.messageOnScreen("The code has been sent to your email account");
                this.dispose();
            }
        }

    }//GEN-LAST:event_btnSubmitMouseClicked

    private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
        exit();
    }//GEN-LAST:event_jLabel5MouseClicked

    private void jPanel2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MousePressed

        // get point of this screen
        pointX = evt.getX();
        pointY = evt.getY();
    }//GEN-LAST:event_jPanel2MousePressed

    private void jPanel2MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseDragged

        // move this screen 
        this.setLocation(this.getLocation().x + evt.getX() - pointX,
                this.getLocation().y + evt.getY() - pointY);
    }//GEN-LAST:event_jPanel2MouseDragged

    private void lblbMinMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblbMinMouseClicked

        // min this window
        this.setState(JFrame.ICONIFIED);
    }//GEN-LAST:event_lblbMinMouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        exit();
    }//GEN-LAST:event_formWindowClosing

    private void LblRegisterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblRegisterMouseClicked

        registerScreen lg = new registerScreen();
        lg.setVisible(true);
        exit();
    }//GEN-LAST:event_LblRegisterMouseClicked

    private void lblLoginMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblLoginMouseClicked
        
        loginScreen lg = new loginScreen();
        lg.setVisible(true);
        exit();
    }//GEN-LAST:event_lblLoginMouseClicked

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(forgotPassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(forgotPassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(forgotPassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(forgotPassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>


        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new forgotPassword().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LblMail;
    private javax.swing.JLabel LblRegister;
    private javax.swing.JButton btnSubmit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblLogin;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblbMin;
    private javax.swing.JTextField txtEmail;
    // End of variables declaration//GEN-END:variables
}
