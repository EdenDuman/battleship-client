package screens;

import battleship.client.serverFunctions;
import battleship.client.seyHello;
import com.sbix.jnotify.NPosition;
import com.sbix.jnotify.NoticeWindow;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class registerScreen extends javax.swing.JFrame {

    private serverFunctions server;
    private String ip;
    private String mac;
    private int id;
    private int pointX;
    private int pointY;
    private boolean flag;
    public Thread hello;

    private void start() {
        
        ImageIcon imgThisImg = new ImageIcon(getClass().getResource("/icons/applicationIcon.jpg"));
        this.setIconImage(imgThisImg.getImage());
        this.server = new serverFunctions();
        this.ip = this.server.getip();
        this.mac = this.server.getmac();
        this.id = 0;
        this.pointX = 0;
        this.pointY = 0;
        this.flag = false;
        this.hello = null;
    }

    private void exit() {

        if (this.id > 0) {
            server.logout(id, ip, mac);
        }

        this.dispose();
    }

    private void clear() {
        
        txtNickName.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
        txtEmail.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
        txtPassword.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
        txtRe_Password.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
    }

    private boolean checkdata() {

        boolean flag = true;

        if (txtNickName.getText().trim().equals("")) {
            txtNickName.setBorder(BorderFactory.createLineBorder(Color.RED,1));
            flag = false;
        }

        if (txtEmail.getText().trim().equals("")) {
            txtEmail.setBorder(BorderFactory.createLineBorder(Color.RED,1));
            flag = false;
        }

        if (txtPassword.getText().trim().equals("")) {
            txtPassword.setBorder(BorderFactory.createLineBorder(Color.RED,1));
            flag = false;
        } else {
            if(!txtPassword.getText().trim().
                    equals(txtRe_Password.getText().trim())){
                txtRe_Password.setBorder(BorderFactory.createLineBorder(Color.RED,1));
                flag = false;
            }
        }
        return flag;
    }
    
    private void checkResult(){
        
        String message = "";
        switch(this.id){
            case -1:
                message = "Email already exists Use another email";
                break;
            case -2:
                message = "communication problem";
                break;
        }
        if(this.id < 0){
            new NoticeWindow(Color.red, message, NoticeWindow.SHORT_DELAY, NPosition.CENTER);
        }
    }

    public registerScreen() {
        start();
        initComponents();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lblClose = new javax.swing.JLabel();
        lblMin = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        LblMail = new javax.swing.JLabel();
        LblRe_password = new javax.swing.JLabel();
        lbltPassword = new javax.swing.JLabel();
        LblNickName = new javax.swing.JLabel();
        txtNickName = new javax.swing.JTextField();
        txtPassword = new javax.swing.JPasswordField();
        txtRe_Password = new javax.swing.JPasswordField();
        txtEmail = new javax.swing.JTextField();
        btnSubmit = new javax.swing.JButton();
        lblLogin = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBackground(new java.awt.Color(44, 62, 80));
        jPanel2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel2MouseDragged(evt);
            }
        });
        jPanel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel2MousePressed(evt);
            }
        });

        lblClose.setBackground(new java.awt.Color(255, 255, 255));
        lblClose.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lblClose.setForeground(new java.awt.Color(255, 51, 51));
        lblClose.setText("X");
        lblClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblCloseMouseClicked(evt);
            }
        });

        lblMin.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lblMin.setForeground(new java.awt.Color(255, 51, 51));
        lblMin.setText(" - ");
        lblMin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblMinMouseClicked(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Bernard MT Condensed", 1, 30)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(162, 207, 149));
        jLabel1.setText("Register");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 416, Short.MAX_VALUE)
                .addComponent(lblMin)
                .addGap(25, 25, 25)
                .addComponent(lblClose, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblMin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(5, 5, 5))
                    .addComponent(lblClose, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(29, 29, 29))
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 698, -1));

        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblMail.setFont(new java.awt.Font("Bernard MT Condensed", 0, 18)); // NOI18N
        LblMail.setForeground(new java.awt.Color(162, 207, 149));
        LblMail.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMail.setText("Email");
        LblMail.setToolTipText("");
        jPanel3.add(LblMail, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 310, 100, 50));

        LblRe_password.setFont(new java.awt.Font("Bernard MT Condensed", 0, 18)); // NOI18N
        LblRe_password.setForeground(new java.awt.Color(162, 207, 149));
        LblRe_password.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblRe_password.setText("Re_Password");
        jPanel3.add(LblRe_password, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 470, 100, 50));

        lbltPassword.setFont(new java.awt.Font("Bernard MT Condensed", 0, 18)); // NOI18N
        lbltPassword.setForeground(new java.awt.Color(162, 207, 149));
        lbltPassword.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbltPassword.setText("Password");
        lbltPassword.setToolTipText("");
        jPanel3.add(lbltPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 390, 100, 50));

        LblNickName.setFont(new java.awt.Font("Bernard MT Condensed", 0, 18)); // NOI18N
        LblNickName.setForeground(new java.awt.Color(162, 207, 149));
        LblNickName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblNickName.setText("NickName");
        jPanel3.add(LblNickName, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 240, 100, 50));

        txtNickName.setFont(new java.awt.Font("Bernard MT Condensed", 1, 20)); // NOI18N
        txtNickName.setForeground(new java.awt.Color(162, 207, 149));
        txtNickName.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtNickName.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtNickName.setOpaque(false);
        jPanel3.add(txtNickName, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 240, 260, 50));

        txtPassword.setFont(new java.awt.Font("Bernard MT Condensed", 1, 20)); // NOI18N
        txtPassword.setForeground(new java.awt.Color(162, 207, 149));
        txtPassword.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPassword.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtPassword.setOpaque(false);
        jPanel3.add(txtPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 390, 260, 50));

        txtRe_Password.setFont(new java.awt.Font("Bernard MT Condensed", 1, 20)); // NOI18N
        txtRe_Password.setForeground(new java.awt.Color(162, 207, 149));
        txtRe_Password.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtRe_Password.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtRe_Password.setOpaque(false);
        jPanel3.add(txtRe_Password, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 470, 260, 50));

        txtEmail.setFont(new java.awt.Font("Bernard MT Condensed", 1, 20)); // NOI18N
        txtEmail.setForeground(new java.awt.Color(162, 207, 149));
        txtEmail.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtEmail.setToolTipText("");
        txtEmail.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtEmail.setOpaque(false);
        jPanel3.add(txtEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 310, 260, 50));

        btnSubmit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/‏‏submitIMG.png"))); // NOI18N
        btnSubmit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSubmitMouseClicked(evt);
            }
        });
        jPanel3.add(btnSubmit, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 600, 130, 60));

        lblLogin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/loginIMG.png"))); // NOI18N
        lblLogin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblLoginMouseClicked(evt);
            }
        });
        jPanel3.add(lblLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 600, 130, 60));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/backgroundScreen2.jpg"))); // NOI18N
        jPanel3.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 700, 700));

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 88, 700, 700));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSubmitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubmitMouseClicked

        if(this.flag){
            return;
        }
        
        clear();
        if (checkdata()) {
            String nickname = txtNickName.getText().trim();
            String mail = txtEmail.getText().trim();
            String password = txtPassword.getText().trim();
            
            this.id = server.registrar(ip, mac, nickname, mail, password);
            
            checkResult();
            
            if(this.id > 0){
                hello = new Thread(new seyHello(this.id));
                hello.start();
                this.btnSubmit.setEnabled(false);
                this.lblLogin.setEnabled(false);
                mainScreen ms = new mainScreen();
                ms.id = this.id;
                ms.hello = this.hello;
                ms.setName();
                ms.setVisible(true);
                this.dispose();
            }
        }

    }//GEN-LAST:event_btnSubmitMouseClicked

    private void lblCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCloseMouseClicked
        exit();
    }//GEN-LAST:event_lblCloseMouseClicked

    private void jPanel2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MousePressed

        // get point of this screen
        pointX = evt.getX();
        pointY = evt.getY();
    }//GEN-LAST:event_jPanel2MousePressed

    private void jPanel2MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseDragged

        // move this screen 
        this.setLocation(this.getLocation().x + evt.getX() - pointX,
                this.getLocation().y + evt.getY() - pointY);
    }//GEN-LAST:event_jPanel2MouseDragged

    private void lblMinMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblMinMouseClicked

        // min this window
        this.setState(JFrame.ICONIFIED);
    }//GEN-LAST:event_lblMinMouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        exit();
    }//GEN-LAST:event_formWindowClosing

    private void lblLoginMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblLoginMouseClicked
        
        if(this.flag){
            return;
        }
        
        loginScreen ls = new loginScreen();
        ls.setVisible(true);
        exit();
    }//GEN-LAST:event_lblLoginMouseClicked

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(registerScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(registerScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(registerScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(registerScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>


        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new registerScreen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LblMail;
    private javax.swing.JLabel LblNickName;
    private javax.swing.JLabel LblRe_password;
    private javax.swing.JButton btnSubmit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lblClose;
    private javax.swing.JLabel lblLogin;
    private javax.swing.JLabel lblMin;
    private javax.swing.JLabel lbltPassword;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtNickName;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JPasswordField txtRe_Password;
    // End of variables declaration//GEN-END:variables
}
