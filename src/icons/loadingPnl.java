/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package icons;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import static javax.swing.JComponent.TOOL_TIP_TEXT_KEY;
import javax.swing.JPanel;

/**
 *
 * @author Eden
 */
public class loadingPnl extends  JPanel{
    
    private double progress_value;
    private int time = 0;
    
    public void abc(double a,int time) {
        progress_value = a;
        this.time = time;
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.translate(this.getWidth() / 2, this.getHeight() / 2);
        g2.rotate(Math.toRadians(270));

        Arc2D.Float arc = new Arc2D.Float(Arc2D.PIE);

        Ellipse2D circle = new Ellipse2D.Double(0, 0, 80, 80);
        arc.setFrameFromCenter(new Point(0, 0), new Point(90, 90));
        circle.setFrameFromCenter(new Point(0, 0), new Point(80, 80));

        arc.setAngleStart(1);
        arc.setAngleExtent(-progress_value * 3.6);
        g2.setColor(new Color(162,207,149,190));
        g2.draw(arc);
        g2.fill(arc);

        //162,207,149
        g2.setColor(new Color(16,32,57,0));
        g2.draw(circle);
        g2.fill(circle);

        g2.setColor(Color.WHITE);

        g2.rotate(Math.toRadians(90));
        g.setFont(new Font(TOOL_TIP_TEXT_KEY, Font.PLAIN, 60));

        FontMetrics fm = g2.getFontMetrics();
        Rectangle2D r = fm.getStringBounds(" "+time+" ", g);

        int x = (0 - (int) r.getWidth() / 2);
        int y = (0 - (int) r.getHeight() / 2 + fm.getAscent());
        g2.drawString(" "+time+" ", x, y);
        
    }

    

}
