package battleship.client;

import javax.swing.ImageIcon;


public class Tools extends Location{
    
    private ImageIcon image;

    public Tools() {
        super();
        this.image = null;
    }

    public Tools(ImageIcon image) {
        super();
        this.image = image;
    }

    public Tools(ImageIcon image, int pointx, int pointy) {
        super(pointx, pointy);
        this.image = image;
    }
    
    public void showOnBoard(){
        
    }

    public ImageIcon getImage() {
        return image;
    }

    public void setImage(ImageIcon image) {
        this.image = image;
    }

}
