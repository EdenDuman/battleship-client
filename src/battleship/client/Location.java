package battleship.client;


public class Location {
    
    private int pointx;
    private int pointy;

    public Location() {
        this.pointx = -1;
        this.pointy = -1;
    }

    public Location(int pointx, int pointy) {
        if(pointx > 9){
            pointx = 9;
        }
        if(pointy > 9){
            pointy = 9;
        }
        if(pointx < 0){
            pointx = 0;
        }
        if(pointy < 0){
            pointy = 0;
        }
        this.pointx = pointx;
        this.pointy = pointy;
    }

    public int getPointx() {
        return pointx;
    }

    public void setPointx(int pointx) {
        this.pointx = pointx;
    }

    public int getPointy() {
        return pointy;
    }

    public void setPointy(int pointy) {
        this.pointy = pointy;
    }
    
    @Override
    public String toString(){
        return "To String " + this.pointx + " , " + this.pointy;
    }
    
    
}
