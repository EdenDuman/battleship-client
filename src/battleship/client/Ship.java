/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship.client;


import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;

/**
 *
 * @author Eden
 */
public class Ship extends Tools{
    
    private int shipID;
    private int size;
    private boolean isOrtogonal;
    private boolean isInLife;
    private List<ImageIcon> ImagesToShow;

    public Ship() {
        super();
        this.shipID = -1;
        this.size = -1;
        this.isOrtogonal = false;
        this.isInLife = false;
        this.ImagesToShow = new ArrayList<ImageIcon>();
    }

    public Ship(int shipID, int size, boolean isOrtogonal, boolean isInLife,
            List<ImageIcon> ImagesToShow) {
        super();
        this.shipID = shipID;
        this.size = size;
        this.isOrtogonal = isOrtogonal;
        this.isInLife = isInLife;
        this.ImagesToShow = ImagesToShow;
    }

    

    public Ship(int shipID, int size, boolean isOrtogonal, boolean isInLife,
            List<ImageIcon> ImagesToShow, ImageIcon image) {
        super(image);
        this.shipID = shipID;
        this.size = size;
        this.isOrtogonal = isOrtogonal;
        this.isInLife = isInLife;
        this.ImagesToShow = ImagesToShow;
    }

    public Ship(int shipID, int size, boolean isOrtogonal, boolean isInLife,
                ImageIcon image, int pointx, int pointy, List<ImageIcon> ImagesToShow) {
        super(image, pointx, pointy);
        this.shipID = shipID;
        this.size = size;
        this.isOrtogonal = isOrtogonal;
        this.isInLife = isInLife;
        this.ImagesToShow = ImagesToShow;
    }

    public Ship(Ship ship){
        super(ship.getImage(), ship.getPointx(), ship.getPointy());
        this.shipID = ship.getShipID();
        this.size = ship.getSize();
        this.isOrtogonal = ship.isIsOrtogonal();
        this.isInLife = ship.isInLife;
        this.ImagesToShow = ship.getImagesToShow();
    }
    
    

    @Override
    public void showOnBoard(){
        
    }

    public int getShipID() {
        return shipID;
    }

    public void setShipID(int shipID) {
        this.shipID = shipID;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isIsOrtogonal() {
        return isOrtogonal;
    }

    public void setIsOrtogonal(boolean isOrtogonal) {
        this.isOrtogonal = isOrtogonal;
    }

    public boolean isInLife() {
        return isInLife;
    }

    public void setIsInLife(boolean isInLife) {
        this.isInLife = isInLife;
    }

    public ImageIcon getImagesToShow(int i) {
        return this.ImagesToShow.get(i);
    }
    
    public List<ImageIcon> getImagesToShow() {
        return this.ImagesToShow;
    }

    public void setImagesToShow(List<ImageIcon> ImagesToShow) {
        this.ImagesToShow = ImagesToShow;
    }    
    
    @Override
    public String toString(){
        return "ID: " + shipID +
                "\nsize: " + size +
                "\n " + super.toString();
    }
    
}
