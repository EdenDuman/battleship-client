
package battleship.client;

/**
 *
 * @author Eden
 */
public class CheckWhatNew implements Runnable{

    private GameLogic gl;
    private String lastMove;

    public CheckWhatNew(GameLogic gl) {
        this.gl = gl;
        this.lastMove = "";
    }    
    
    @Override
    public void run() {
        String result = gl.server.checkGameUpdates(gl.userID, gl.gameID);
        result = result.trim();
        
        while (!result.equals("-1") && !result.equals("2")) {            
            if(!result.equals("0") && !result.equals(this.lastMove)){
                this.lastMove = result;
                gl.enemyGuess(result);
            }
            result = gl.server.checkGameUpdates(gl.userID, gl.gameID);
            result = result.trim();
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
            }
        }
        if(result.equals("-1")){
            gl.lose();
        }
        else if (result.equals("2")){
            gl.win();
        }
        else{
            gl.enemyGuess(result);
        }
        
    }
    
}
