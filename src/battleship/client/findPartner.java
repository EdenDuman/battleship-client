package battleship.client;

import screens.mainScreen;
import screens.gameScreen;

/**
 * this class check all 3 secend if a game partner is found The class died when
 * a partner was found
 *
 * @author Eden
 */
public class findPartner implements Runnable {

    private int gameID;
    private mainScreen ms;
    private int guest;
    private serverFunctions server;

    public findPartner(mainScreen ms) {
        this.gameID = ms.gameID;
        this.ms = ms;
        this.guest = 0;
        this.server = new serverFunctions();
    }

    @Override
    public void run() {

        while (this.guest == 0) {
            this.guest = server.lookForAPartner(this.gameID);
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {

            }

        }

        //Moves to a game board
        gameScreen gs = new gameScreen();
        gs.hello = this.ms.hello;
        gs.myID = this.ms.id;
        gs.gameID = this.gameID;
        gs.logic = new GameLogic(gs);
        gs.LblmyName.setText(server.getNickName(this.ms.id));
        gs.LblEnemyName.setText(server.getNickName(this.guest));
        gs.guestName = server.getNickName(this.guest);
        gs.startClock();
        gs.setVisible(true);
        this.ms.dispose();
    }

}
