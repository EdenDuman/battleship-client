/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship.client;

import java.awt.Color;

/**
 *
 * @author Eden
 */
public class hasTheGameStarted implements Runnable{

    private GameLogic gl;
    private boolean itIsMyTurn = false;

    public hasTheGameStarted(GameLogic gl) {
        this.gl = gl;
    }
    
    @Override
    public void run() {
    
        int result = gl.server.hasTheGameStarted(gl.userID, gl.gameID);
        if(0 == result){
            gl.showMsg(Color.BLUE, "Waiting for the other player");
            itIsMyTurn = true;
        }
        
        while (0 == result) {
            //System.out.println("battleship.client.hasTheGameStarted.run()");            
            result = gl.server.hasTheGameStarted(gl.userID, gl.gameID);
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
            }
        }

        switch(result){
            case 1:
                gl.startPlay(itIsMyTurn);
                break;
            case 2:
                gl.win();
                break;
        }
    }
    
}
