package battleship.client;

import com.sbix.jnotify.NPosition;
import com.sbix.jnotify.NoticeWindow;
import encryptionapi.Encryption;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * This class communicates with the server
 *
 * @author Eden
 */
public class serverFunctions {

    private Socket socketOfClient;
    private BufferedReader reader;
    private PrintStream cout;

    private void createConnection() {

        try {
            socketOfClient = new Socket("localhost", 5000);
            reader = new BufferedReader(new InputStreamReader(socketOfClient.getInputStream()));
            cout = new PrintStream(socketOfClient.getOutputStream());
        } catch (IOException ex) {
            String message = "NO CONNECTION TO SERVER";
            new NoticeWindow(Color.RED, message, NoticeWindow.SHORT_DELAY, NPosition.CENTER);
        }
    }

    private void close() {

        try {
            reader.close();
            cout.close();
            socketOfClient.close();

        } catch (IOException | NullPointerException ex) {
            String message = "NO CONNECTION TO SERVER";
            new NoticeWindow(Color.RED, message, NoticeWindow.SHORT_DELAY, NPosition.CENTER);
        }

    }

    private String getStringFromServer() {

        String str = "";
        try {
            if (reader == null) {
                String message = "NO CONNECTION TO SERVER";
                new NoticeWindow(Color.RED, message, NoticeWindow.SHORT_DELAY, NPosition.CENTER);
                return "";
            }
            str = reader.readLine();
            str = Encryption.open(str);
        } catch (IOException ex) {
            str = "-1";
            close();
        }
        return str;
    }

    private int getIntFromServer() {

        String str = "";
        int mag = 0;
        try {
            if (reader == null) {
                String message = "NO CONNECTION TO SERVER";
                new NoticeWindow(Color.RED, message, NoticeWindow.SHORT_DELAY, NPosition.CENTER);
                return -500;
            }
            str = reader.readLine();
            str = Encryption.open(str);
            mag = Integer.parseInt(str);
        } catch (IOException ex) {
            mag = -1;
            close();
        }
        return mag;
    }

    private void setDataToServer(String str) {

        str = Encryption.close(str);
        if (cout == null) {
            String message = "NO CONNECTION TO SERVER";
            new NoticeWindow(Color.RED, message, NoticeWindow.SHORT_DELAY, NPosition.CENTER);
            return;
        }
        cout.println(str);
        cout.flush();
    }

    public int registrar(String ip, String mac, String nickname, String mail,
            String password) {

        createConnection();

        setDataToServer("registrar");

        setDataToServer(ip);
        setDataToServer(mac);
        setDataToServer(nickname);
        setDataToServer(mail);
        setDataToServer(password);

        int result = getIntFromServer();

        close();

        return result;

    }

    public int login(String ip, String mac, String password, String mail) {

        createConnection();

        setDataToServer("login");

        setDataToServer(mail);
        setDataToServer(password);
        setDataToServer(ip);
        setDataToServer(mac);

        int result = getIntFromServer();

        close();

        return result;

    }

    public int logout(int id, String ip, String mac) {

        createConnection();

        setDataToServer("logout");

        setDataToServer(id + "");
        setDataToServer(ip);
        setDataToServer(mac);

        int result = getIntFromServer();

        close();

        return result;

    }

    public int[] findGame(int id) {

        createConnection();

        setDataToServer("findGame");

        setDataToServer(id + "");

        int result[] = new int[2];
        result[0] = getIntFromServer();
        result[1] = getIntFromServer();

        close();

        return result;

    }

    public String setMyBoard(int userID, int gameID, String board) {

        createConnection();

        setDataToServer("setMyBoard");

        setDataToServer(userID + "");
        setDataToServer(gameID + "");
        setDataToServer(board);

        String result = getStringFromServer();

        close();

        return result;

    }

    public int resetPassword(String mail, String ip, String mac) {

        createConnection();

        setDataToServer("resetPassword");

        setDataToServer(mail);
        setDataToServer(ip);
        setDataToServer(mac);

        int result = getIntFromServer();

        close();

        return result;

    }

    public String getip() {
        return "80.246.141.254";
    }

    public String getmac() {

        String result = "";
        try {
            // get all network interfaces of the current system
            Enumeration<NetworkInterface> networkInterface
                    = NetworkInterface.getNetworkInterfaces();

            //Prepares variables
            byte[] macAddressBytes = null;
            NetworkInterface network = null;

            // iterate over all interfaces and choose a network 
            while (networkInterface.hasMoreElements()) {

                network = networkInterface.nextElement();

                //System.out.println(network);
                if (network.getName().equals("wlan0")) {
                    macAddressBytes = network.getHardwareAddress();
                    break;
                }
                networkInterface.nextElement();
            }

            // Build a Mac Address
            if (macAddressBytes == null) {
                result = "00-00-00-00-00-00";
            } else {

                // initialize a string builder to hold mac address
                StringBuilder macAddressStr = new StringBuilder();

                // iterate over the bytes of mac address  
                for (int i = 0; i < macAddressBytes.length; i++) {

                    // convert byte to string in hexadecimal form
                    macAddressStr.append(String.format("%02X", macAddressBytes[i]));

                    // check if there are more bytes,
                    // then add a "-" to make it more readable
                    if (i < macAddressBytes.length - 1) {
                        macAddressStr.append("-");
                    }
                }
                result = macAddressStr.toString();
            }

        } catch (SocketException e) {
            result = "00-00-00-00-00-00";
        }
        return result;
    }

    public int sayHello(int id) {

        createConnection();

        setDataToServer("hello");

        setDataToServer(id + "");

        int result = getIntFromServer();

        close();

        return result;
    }

    /**
     *
     * @param userID
     * @param move
     * @param gameID
     * @return
     */
    public int setMove(int userID, int gameID, String move) {

        createConnection();

        setDataToServer("setMove");

        setDataToServer(userID + "");
        setDataToServer(gameID + "");
        setDataToServer(move);

        int result = getIntFromServer();

        close();

        return result;
    }

    public int lookForAPartner(int userID) {

        createConnection();

        setDataToServer("LookForAPartner");

        setDataToServer(userID + "");

        int result = getIntFromServer();

        close();

        return result;

    }

    /**
     * under construction
     *
     * @param userID
     * @param gameID
     * @return
     */
    public String checkGameUpdates(int userID, int gameID) {

        createConnection();

        setDataToServer("checkGameUpdates");

        setDataToServer(userID + "");
        setDataToServer(gameID + "");

        String result = getStringFromServer();

        close();

        return result;

    }

    public String getNickName(int userID) {

        createConnection();

        setDataToServer("getNickName");

        setDataToServer(userID + "");

        String result = getStringFromServer();

        close();

        return result;
    }

    public void FinishedGame(int gameID) {

        createConnection();

        setDataToServer("FinishedGame");

        setDataToServer(gameID + "");

        close();
    }

    public int hasTheGameStarted(int userID, int gameID) {

        createConnection();

        setDataToServer("hasTheGameStarted");

        setDataToServer(userID + "");
        setDataToServer(gameID + "");

        int result = getIntFromServer();

        close();

        return result;
    }

    public ArrayList<String> getTop3Player() {

        createConnection();

        setDataToServer("top3Player");

        ArrayList<String> result = new ArrayList<>();

        for (int i = 0; i < 9; i++) {
            result.add(getStringFromServer());
        }

        close();

        return result;
    }

    public String changePassword(int userID, String oldPassword,
            String newPassword, String ip, String mac) {

        createConnection();

        setDataToServer("changePassword");

        setDataToServer(userID + "");
        setDataToServer(oldPassword);
        setDataToServer(newPassword);
        setDataToServer(ip);
        setDataToServer(mac);

        String result = getStringFromServer();

        close();

        return result;
    }

    public int timeOut(int userID, int gameID) {

        createConnection();

        setDataToServer("timeOut");

        setDataToServer(userID + "");
        setDataToServer(gameID + "");

        int result = getIntFromServer();

        close();

        return result;
    }

}
