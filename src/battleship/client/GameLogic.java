package battleship.client;

import screens.mainScreen;
import screens.gameScreen;
import com.sbix.jnotify.NPosition;
import com.sbix.jnotify.NoticeWindow;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;

public class GameLogic {

    // Arrow direction
    public static final int UP = -1;
    public static final int DOWN = 1;
    public static final int RIGHT = 2;
    public static final int LEFT = -2;

    // Board size
    private final int LENGTH = 10;
    private final int WIDTH = 10;

    private int[][] submarineBoard;
    private int[][] Guessboard;
    public serverFunctions server;
    public int userID;
    public int gameID;
    public boolean endGame = false;
    public Thread updateGame;
    public gameScreen gs;

    public GameLogic(gameScreen gs) {
        this.submarineBoard = new int[LENGTH][WIDTH];
        this.Guessboard = new int[LENGTH][WIDTH];
        this.server = new serverFunctions();
        this.userID = gs.myID;
        this.gameID = gs.gameID;
        this.gs = gs;
    }

    public void hideDirectionArrows() {

        // gs.lowerPanel.remove(gs.up);
        // gs.lowerPanel.remove(gs.down);
        // gs.lowerPanel.remove(gs.right);
        // gs.lowerPanel.remove(gs.left);
        gs.up.setVisible(false);
        gs.down.setVisible(false);
        gs.right.setVisible(false);
        gs.left.setVisible(false);

    }

    /**
     * the function run on submarineBoard end chek if ther arr free location 
     * To improve performance the function first checks if the ship is heading in
     * the direction
     *
     * @param ship the ship obj
     * @param direction from finl
     * @return Is it legal to assume in that direction
     */
    private boolean isSubmarineLocationValid(Ship ship, int direction) {

        Location startP = new Location();
        Location endP = new Location();
        boolean flag = true;

        switch (direction) {
            case UP:
            case DOWN:

                //// Check if the ship is heading in the direction
                flag = !(ship.getPointy() + (direction * (ship.getSize() - 1)) < 0
                        || ship.getPointy() + (direction * (ship.getSize() - 1)) > 9);

                if (!flag) {
                    return flag;
                }

                // if the ship is heading in the direction
                // check if the place free
                if (direction == UP) {
                    endP = new Location(ship.getPointx() + 1, ship.getPointy() + 1);
                    startP = new Location(ship.getPointx() - 1, ship.getPointy() - ship.getSize());
                }
                if (direction == DOWN) {
                    endP = new Location(ship.getPointx() + 1, ship.getPointy() + ship.getSize());
                    startP = new Location(ship.getPointx() - 1, ship.getPointy() - 1);

                }

                break;

            case RIGHT:
            case LEFT:
                // Reset the direction
                if (direction == RIGHT) {
                    direction -= 1;
                } else {
                    direction += 1;
                }

                // Check if the ship is heading in the direction
                flag = !(ship.getPointx() + (direction * (ship.getSize() - 1)) < 0
                        || ship.getPointx() + (direction * (ship.getSize() - 1)) > 9);

                if (!flag) {
                    return flag;
                }

                // if the ship is heading in the direction
                // check if the place free
                if (direction == (RIGHT - 1)) {
                    endP = new Location(ship.getPointx() + ship.getSize(), ship.getPointy() + 1);
                    startP = new Location(ship.getPointx() - 1, ship.getPointy() - 1);

                }
                if (direction == (LEFT + 1)) {
                    endP = new Location(ship.getPointx() + 1, ship.getPointy() + 1);
                    startP = new Location(ship.getPointx() - ship.getSize(), ship.getPointy() - 1);
                }

                break;
        }

        int sum = 0;
        for (int i = startP.getPointx(); i <= endP.getPointx(); i++) {
            for (int j = startP.getPointy(); j <= endP.getPointy(); j++) {
                sum += submarineBoard[i][j];
            }
        }

        flag = (sum == 0);
        return flag;
    }

    /**
     * check 4 directions to add the ship If it is possible to add the ship the
     * function will paint the appropriate label If the ship cannot be added the
     * function returns false
     *
     * @param ship Initialized with location and size
     * @return Is it possible to add the ship
     */
    public boolean showValidArrows(Ship ship) {

        boolean isIn = false;
        
        // Protection code
        if (ship.getShipID() == -1) {
            return isIn;
        }

        // clear the arrows labels
        hideDirectionArrows();

        // check 4 directions
        if (isSubmarineLocationValid(ship, UP)) {
            gs.up.setVisible(true);
            isIn = true;
        }
        if (isSubmarineLocationValid(ship, DOWN)) {
            gs.down.setVisible(true);
            isIn = true;
        }
        if (isSubmarineLocationValid(ship, RIGHT)) {
            gs.right.setVisible(true);
            isIn = true;
        }
        if (isSubmarineLocationValid(ship, LEFT)) {
            gs.left.setVisible(true);
            isIn = true;
        }

        // If the ship cannot be added
        return isIn;
    }

    /**
     * Place the submarine in the selected location and direction
     *
     * @param ship
     * @param direction
     */
    public void showSubmarineOnBoard(Ship ship, int direction) {

        // Protection code
        if (ship.getShipID() == -1) {
            return;
        }

        int countToRotate = 0;
        ImageIcon image;

        switch (direction) {
            case UP:
                countToRotate = 2;
                break;
            case DOWN:
                countToRotate = 0;
                break;
            case RIGHT:
                countToRotate = 3;
                break;
            case LEFT:
                countToRotate = 1;
                break;
        }
        
        switch (direction) {
            case UP:
            case DOWN:
                for (int i = 0; i < ship.getSize(); i++) {
                    submarineBoard[ship.getPointx()][ship.getPointy() + (i * direction)]
                            = ship.getShipID();
                    image = rotate(ship.getImagesToShow(i), (countToRotate * 90.0));
                    gs.myBoard[ship.getPointx()][ship.getPointy() + (i * direction)].setIcon(
                            image);
                    gs.myBoard[ship.getPointx()][ship.getPointy() + (i * direction)].setBorder(null);
                }
                break;
                
            case RIGHT:
            case LEFT:

                // Reset the direction
                if (direction == RIGHT) {
                    direction -= 1;
                } else {
                    direction += 1;
                }

                for (int i = 0; i < ship.getSize(); i++) {
                    submarineBoard[ship.getPointx() + (i * direction)][ship.getPointy()]
                            = ship.getShipID();
                    image = rotate(ship.getImagesToShow(i), (countToRotate * 90.0));
                    gs.myBoard[ship.getPointx() + (i * direction)][ship.getPointy()].setIcon(
                            image);
                    gs.myBoard[ship.getPointx() + (i * direction)][ship.getPointy()].setBorder(null);
                }
                break;
        }

    }
    
    public boolean putShipInRandumLocation(Ship ship){
        
        if(isSubmarineLocationValid(ship, UP)){
            showSubmarineOnBoard(ship, UP);
            return true;
        }
        
        if(isSubmarineLocationValid(ship, RIGHT)){
            showSubmarineOnBoard(ship, RIGHT);
            return true;
        }
        
        if(isSubmarineLocationValid(ship, DOWN)){
            showSubmarineOnBoard(ship, DOWN);
            return true;
        }
        
        if(isSubmarineLocationValid(ship, LEFT)){
            showSubmarineOnBoard(ship, LEFT);
            return true;
        }
        
        return false;
    }

    /**
     * A function that receives and rotates an image
     *
     * @param image
     * @param degrees
     * @return ImageIcon pic after rotate
     */
    public ImageIcon rotate(ImageIcon image, Double degrees) {

        /*
        another way to rotate a photo
        
        AffineTransform tx = new AffineTransform();
        tx.rotate(1.57, image.getWidth() / 2, image.getHeight() / 2);

        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
        image = op.filter(image, null);
         */
        BufferedImage bi = new BufferedImage(image.getIconWidth(),
                image.getIconHeight(),
                BufferedImage.TYPE_INT_RGB);
        Graphics g = bi.createGraphics();

        // paint the Icon to the BufferedImage.
        image.paintIcon(null, g, 0, 0);
        g.dispose();

        // Calculate the new size of the image based on the angle of rotaion
        double radians = Math.toRadians(degrees);
        double sin = Math.abs(Math.sin(radians));
        double cos = Math.abs(Math.cos(radians));
        int newWidth = (int) Math.round(bi.getWidth() * cos + bi.getHeight() * sin);
        int newHeight = (int) Math.round(bi.getWidth() * sin + bi.getHeight() * cos);

        // Create a new image
        BufferedImage rotate = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = rotate.createGraphics();

        // Calculate the "anchor" point around which the image will be rotated
        int x = (newWidth - bi.getWidth()) / 2;
        int y = (newHeight - bi.getHeight()) / 2;

        // Transform the origin point around the anchor point
        AffineTransform at = new AffineTransform();
        at.setToRotation(radians, x + (bi.getWidth() / 2), y + (bi.getHeight() / 2));
        at.translate(x, y);
        g2d.setTransform(at);

        // Paint the originl image
        g2d.drawImage(bi, 0, 0, null);
        g2d.dispose();
        return new ImageIcon(rotate);
    }

    private String convertLocationToMark(int i, int j) {
        String result = "";

        switch (i) {
            case 0:
                result = "A";
                break;
            case 1:
                result = "B";
                break;
            case 2:
                result = "C";
                break;
            case 3:
                result = "D";
                break;
            case 4:
                result = "E";
                break;
            case 5:
                result = "F";
                break;
            case 6:
                result = "G";
                break;
            case 7:
                result = "H";
                break;
            case 8:
                result = "I";
                break;
            case 9:
                result = "J";
                break;
        }

        result += j + "";
        return result;
    }

    public String setMyBaord() {

        String subLocation = "";
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (submarineBoard[i][j] != 0) {
                    subLocation += convertLocationToMark(i, j) + ":";
                }
            }
        }
        String result;
        result = server.setMyBoard(this.userID, this.gameID, subLocation);
        
        return result;
    }

    public void enemyGuess(String guess) {

        int pointX = Character.getNumericValue(guess.charAt(0)) - 10;
        int pointY = Character.getNumericValue(guess.charAt(1));
        int flag = Character.getNumericValue(guess.charAt(3));
        Location lc = new Location(pointX, pointY);
        boolean isHit;

        isHit = (flag != 0);

        gs.markEnemyMoveOnSubmarineBoard(lc, isHit);
    }

    public void startPlay(boolean itIsMyTurn) {
        
        gs.turn.setOpaque(true);
        gs.theTameHasStarted = true;
        gs.createGuessBoard();
        if (itIsMyTurn) {
            gs.turn.setBackground(new Color(162, 207, 149));
            gs.turn.setText("YOUR TURN");
        } else {
            gs.turn.setBackground(Color.RED);
            gs.turn.setText("NOT YOUR TURN");

        }
        gs.setItIsMyTurn(itIsMyTurn);
        updateGame = new Thread(new CheckWhatNew(this));
        updateGame.start();
    }

    public void win() {
        
        if (endGame) {
            return;
        }
        
        endGame = true;
        gs.dispose();
        System.out.println("win");
        mainScreen ms = new mainScreen();
        ms.id = this.userID;
        ms.hello = gs.hello;
        ms.setName();
        ms.setVisible(true);
        ms.showMessage(Color.BLUE, "YOU WON");
    }

    public void lose() {
        
        if (endGame) {
            return;
        }
        endGame = true;
        System.out.println("lose");
        mainScreen ms = new mainScreen();
        ms.id = this.userID;
        ms.hello = gs.hello;
        ms.setName();
        ms.setVisible(true);
        ms.showMessage(Color.RED, "YOU LOSS");
        gs.dispose();
    }

    public void showMsg(Color color, String message) {
        new NoticeWindow(color, message, NoticeWindow.SHORT_DELAY, NPosition.CENTER);
    }

    public void hit(int i, int j) {
        this.Guessboard[i][j] = 1;
        ImageIcon img;
        img = new ImageIcon(getClass().getResource("/icons/hit.jpg"));
        gs.GuessBoard[i][j].setIcon(img);

    }

    public void miss(int i, int j) {
        this.Guessboard[i][j] = -1;
        ImageIcon img;
        img = new ImageIcon(getClass().getResource("/icons/miss.jpg"));
        gs.GuessBoard[i][j].setIcon(img);
    }

    public void guessLocation(int i, int j) {
        
        if (this.Guessboard[i][j] != 0) {
            this.showMsg(Color.ORANGE, "THIS BUTTON IS ALREADY SELECTED");
            return;
        }

        int result = server.setMove(userID, gameID, convertLocationToMark(i, j));
        switch (result) {
            case 1:
                hit(i, j);
                gs.setItIsMyTurn(true);
                gs.turn.setText("YOUR TURN");
                break;
            case 0:
                miss(i, j);
                gs.setItIsMyTurn(false);
                gs.turn.setBackground(Color.RED);
                gs.turn.setText("NOT YOUR TURN");
                break;
        }
    }

    public void exit() {
        updateGame.interrupt();
    }

    public int[][] getSubmarineBoard() {
        return submarineBoard;
    }

    public int[][] getGuessboard() {
        return Guessboard;
    }

}
