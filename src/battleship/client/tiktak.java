package battleship.client;

import screens.gameScreen;

/**
 *
 * @author Eden
 */
public class tiktak implements Runnable {

    private int time = 60;
    private double delay = (double) 100 / time;
    private gameScreen gs;
    private serverFunctions server;

    public tiktak(gameScreen gs, serverFunctions server) {
        this.gs = gs;
        this.server = server;
    }

    @Override
    public void run() {

        for (int i = 0; i < time; i++) {
            try {
                Thread.sleep(1000);
                gs.loadingPnl1.abc(delay, i + 1);
                gs.loadingPnl1.repaint();
            } catch (InterruptedException ex) {
                System.err.println("error tik tak sleep");
            }
            delay += (double) 100 / time;
        }
        if (this.gs.isItIsMyTurn()) {
            
            if(gs.logic.updateGame != null){
                gs.logic.updateGame.stop();
            }
            
            int result = this.server.timeOut(this.gs.myID, this.gs.gameID);
            if(result == 1){
                gs.logic.win();
            } else {
                gs.logic.lose();
            }
        }
    }
}
